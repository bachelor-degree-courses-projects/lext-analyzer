/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package compiler_project;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javafx.util.Pair;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mojtaba Moazen
 */
public class make_Analysis_table {
    ArrayList<String> main_lines = new ArrayList<>();
    Vector<Pair<String,String>> seprate_grammer=new Vector<>();
    ArrayList<String> grammer_nonterminal=new ArrayList<>();
    List<String> terminals = new ArrayList<>();
    Map<String, Map<String,ArrayList<String>>> first = new HashMap<>();
    Map<String,ArrayList<String>> output=new HashMap<>();
    Map<String, ArrayList<String>> follow=new HashMap<>();
    String start="";
    
    
    public void read_grammer_file(){
        String file_name="C:\\Users\\Mojtaba Moazen\\Desktop\\grammer.conf";
        String line =null;
        
        
        try {
            FileReader fileReader = 
                new FileReader(file_name);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                main_lines.add(line); 
        }
            
            bufferedReader.close();
          
            
    }
        catch(FileNotFoundException ex){
            System.out.println("error reading file " + file_name + " ");
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + file_name + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
        
      seprategrammer();
        
    
    
    
    
}

    private void seprategrammer() {
        
        for (int i=0 ;i<main_lines.size();i++){
            String this_line=main_lines.get(i);
            this_line.replace(" ", "");
            
                String temp=null;
                int j=0;
                while (this_line.charAt(j)!=':') {
                    
                    temp=temp+this_line.charAt(j);
                    j++;
                    
                }
                Pair tmp=new Pair(temp,this_line.substring(j+2)) ;
                seprate_grammer.add(tmp);
                
            
        }
        
        System.out.println(seprate_grammer.get(1).getValue());
        for (int i=0;i<seprate_grammer.size();i++){
            grammer_nonterminal.set(i, seprate_grammer.get(i).getKey().substring(5));
        }
        for (int i=0;i<seprate_grammer.size();i++){
            terminals.set(i, seprate_grammer.get(i).getValue());
        }
        
        
    
    
    
    }
    
  
}