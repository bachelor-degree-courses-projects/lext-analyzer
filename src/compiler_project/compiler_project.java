
package compiler_project;

import compiler_project.NFA;
import static compiler_project.compiler_project.fileInformation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


public class compiler_project {

   
    public static ArrayList<String> fileInformation;
    public static ArrayList<String> fileInformation2;
    public static int count=0;
    private static String inputFileName = "D:\\university\\compiler_project\\lexeme.txt";
    public static NFA[] nfas;
    public static HashMap<String, String> lexemeToken=new HashMap<String, String>();
    private static String final_tokenize ="";
    public static void main(String[] args) throws IOException {
        
        BufferedReader bufferReader=null;
        FileReader fileReader=null;
        fileInformation =new ArrayList<String>();

        readFile(bufferReader, fileReader,inputFileName);
         
        
        nfas = new NFA[fileInformation.size()];
        HashMap<String,String> map = new HashMap<>();
        ArrayList<String> RegExpName=new ArrayList<>();
        for (int i=0 ; i<fileInformation.size() ; i++){
            String[] temp = fileInformation.get(i).split(":=");
            temp[0] = temp[0].trim();
            temp[1] = temp[1].trim();
            MakeNFA mnfa = new MakeNFA(temp[1],map);
            NFA nfa = mnfa.constructNFA();
            map.put(temp[0],temp[1]);
            RegExpName.add(temp[0]);
            nfas[i] = nfa;
        }



        Scanner scanner = new Scanner(System.in);

        
        String input =scanner.nextLine();
        BufferedReader bufferReader1=null;
        FileReader fileReader1=null;
        String inputfile2="D:\\university\\compiler_project\\codeinput.txt";
        ArrayList<String> input_code=readFile2(bufferReader1, fileReader1, inputfile2);
//        while (true){
//            String s = scanner.nextLine();
//            input += s + "\n";
//            if(s.trim().length()==0){
//                break;
//            }
//        }
        input=input.trim();
        DFA[] dfas=new DFA[nfas.length];
        for(int i=0;i<dfas.length;i++){
            dfas[i]=new DFA();
            dfas[i].states.add(DFA.getInitialState(nfas[i]));
        }
        
        int begin = 0;
        int forward = 1;
        int acceptIndex = -1;
        
        boolean[] isrejected=new boolean[dfas.length];
        for(int i=0;i<isrejected.length;i++)
            isrejected[i]=false;
        int value=0;
        while (begin < input.length()){
            String seenInput=input.substring(begin,forward);
            int temp=acceptIndex;
            for(int i=dfas.length-1;i>=0;i--){
                DFA_State currentState=dfas[i].states.get(0);
                for(int j=0;j<seenInput.length();j++){
                    DFA_State nextState=new DFA_State();
                    if(currentState.nextState_match.containsKey(seenInput.charAt(j)+""))
                        nextState=currentState.nextState_match.get(seenInput.charAt(j)+"");
                    else{
                        isrejected[i]=true;
                        break;
                    }
                    DFA_State updatedState=DFA.generateOtherStates(nextState);
                    currentState=updatedState;
                }
                if(currentState.isFinal && !isrejected[i]){
                    acceptIndex=i;
//                    System.out.println("index:  "+i+"  by char  "+seenInput.charAt(seenInput.length()-1)+"  accpetedIndex  "+acceptIndex);
                }
            }
            boolean flag=false;
            for(int i=0;i<isrejected.length;i++){
                if(!isrejected[i]){
                    flag=true;
                    break;
                }
            } 
            //System.out.println("string:  "+seenInput+" temp "+temp+" acceptIndex "+acceptIndex+"  forward  "+forward+" begin "+begin+"  flag  "+flag);
            if(temp==acceptIndex && !flag){
                forward--;
                //System.out.println("here");
                seenInput=input.substring(begin,forward);
                if(acceptIndex!=-1){
                    if(RegExpName.get(acceptIndex) != " "){
                        String tmp = RegExpName.get(acceptIndex);
                        tmp = tmp.trim();
                        
                        System.out.print(tmp.length());
                        System.out.print("!!! ");
                        System.out.println(tmp);
                        final_tokenize+=tmp;
                        final_tokenize+=" ";
                        lexemeToken.put(seenInput, tmp);
                    }
                }
                else{
                    String space = " ";
                    if(!input.substring(begin, forward+1).equals(space)){
                        String tmp = input.substring(begin, forward+1).trim();
                        System.out.print(tmp.length());
                        System.out.print("@@@ ");
                        System.out.println(tmp);
                        final_tokenize+=tmp;
                        final_tokenize+=" ";
                        begin++;
                        forward++;
                    }
                    else{
                        begin++;
                        forward++;
                    }
                }
                begin=forward;
                acceptIndex=-1;
            }
            for(int i=0;i<isrejected.length;i++)
                isrejected[i]=false;
            forward+=1;
            if(forward>input.length()){
                forward--;
               if(acceptIndex!=-1){
                    System.out.println(RegExpName.get(acceptIndex)+"  "+input.substring(begin,forward)+"  lastIF  ");
                    lexemeToken.put(input.substring(begin,forward), RegExpName.get(acceptIndex));
               }
                break;
            }
        }
        
             BufferedWriter  writer=new BufferedWriter(new FileWriter("output.txt"));
         writer.write(final_tokenize);
         writer.close();
         System.out.println(lexemeToken.get("hello"));
        
        
    make_Analysis_table mk = new make_Analysis_table();
//    mk.read_grammer_file();
        final_tokenize.replaceAll("  ", " ");
        System.out.println(final_tokenize);
         try {
            //first arguments is the file in which the grammar is present
            String grammarFileName = "D:\\university\\compiler_project\\grammer.conf";
            
        
            //second file: input strings
            String inputFileName ="D:\\university\\compiler_project\\output.txt";
            BufferedReader br = new BufferedReader(new FileReader(grammarFileName));
            String line;

            List<String> list = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
            br.close();

            String[] lines = list.toArray(new String[0]);
            LL1_Grammer ll1grammer=new LL1_Grammer();
            
            Grammar grammar = ll1grammer.constructGrammar(lines);
            ll1grammer.printGrammar(grammar);

           
            LL1Parser ll1Parser = ll1grammer.constructLL1Parser(grammar);
            System.out.println("Table: " + ll1Parser.ll1ParsingTable);

            br = new BufferedReader(new FileReader(inputFileName));
            list = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
            br.close();

            String[] inputLines = list.toArray(new String[0]);

   
            boolean result = ll1grammer.parseInput(inputLines, ll1Parser);
            System.out.println("Parse result: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    
    
    }
    
    public static void readFile(BufferedReader bufferReader,FileReader fileReader,String inputfilename){ 
        String sCurrentLine;
        try{
            fileReader=new FileReader(inputfilename);
            bufferReader=new BufferedReader(fileReader);
            while((sCurrentLine=bufferReader.readLine())!=null){
                System.out.println(sCurrentLine.trim());
                
                fileInformation.add(sCurrentLine.trim());
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(bufferReader!=null)
                    bufferReader.close();
                if(fileReader!=null)
                    fileReader.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }
     public static ArrayList<String> readFile2(BufferedReader bufferReader,FileReader fileReader,String inputfilename){ 
        String sCurrentLine;
        fileInformation2=new ArrayList<>();
        try{
            fileReader=new FileReader(inputfilename);
            bufferReader=new BufferedReader(fileReader);
            while((sCurrentLine=bufferReader.readLine())!=null){
                System.out.println(sCurrentLine.trim());
                
                fileInformation2.add(sCurrentLine.trim());
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally{
            try{
                if(bufferReader!=null)
                    bufferReader.close();
                if(fileReader!=null)
                    fileReader.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    return fileInformation2;
     }
}


