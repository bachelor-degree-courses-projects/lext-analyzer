/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler_project;

import java.util.ArrayList;

public class NFA_State implements Cloneable{
    
    public static final int MAX_CHAR=255;
    public boolean isFinal = false ;
    public ArrayList<NFA_State> onChar[] = new ArrayList[MAX_CHAR] ;
    public ArrayList<NFA_State> onEmpty  = new ArrayList() ;
    public String name;
    
    public NFA_State(){
        name=compiler_project.count+"";
        compiler_project.count++;
        
        for(int i=0;i<onChar.length;i++)
            onChar[i]=new ArrayList<NFA_State>();
    }
    
    public void addCharEdge(char c, NFA_State next) {
        onChar[(int)c].add(next) ;
    }
    
    public void addEmptyEdge(NFA_State next){
        onEmpty.add(next);
    }
    
    public boolean matches(String string){
        return matches(string,new ArrayList());
    }
    
    private boolean matches(String string,ArrayList visited){
        if(visited.contains(this))
            return false;
        visited.add(this);
        if(string.length()==0){
            if(isFinal)
                return true;
            for(NFA_State nextByEmpty : onEmpty)
                if(nextByEmpty.matches("",visited))
                    return true;
            return false;
        }
        int currentChar=(int)string.charAt(0);
        for(NFA_State next : onChar[currentChar])
            if(next.matches(string.substring(1)))
                return true;
        for(NFA_State nextByEmpty : onEmpty)
            if(nextByEmpty.matches(string,visited))
                return true;
        return false;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
