
package compiler_project;

import java.util.ArrayList;

public class DFA {
    ArrayList<DFA_State> states;
    
    public DFA(){
        states=new ArrayList<DFA_State>();
    }
    public static final DFA_State getInitialState(NFA nfa){
        DFA_State firstState=new DFA_State();
        NFA_State entry=nfa.entryState;
        firstState.currentStates.add(entry);
        for(int i=0;i<entry.onEmpty.size();i++){
            firstState.currentStates.add(entry.onEmpty.get(i));
            if(entry.onEmpty.get(i).isFinal)
                firstState.isFinal=true;
        }
        for(int i=1;i<firstState.currentStates.size();i++){
            for(int j=0;j<firstState.currentStates.get(i).onEmpty.size();j++){
                if(!firstState.currentStates.contains(firstState.currentStates.get(i).onEmpty.get(j)))
                    firstState.currentStates.add(firstState.currentStates.get(i).onEmpty.get(j));
                else
                    System.out.println("Hi");
                if(firstState.currentStates.get(i).onEmpty.get(j).isFinal)
                   firstState.isFinal=true;  
            }
        }
        for(int i=0;i<NFA_State.MAX_CHAR;i++){
            DFA_State state=new DFA_State();
            for(int j=0;j<firstState.currentStates.size();j++){
                for(int k=0;k<firstState.currentStates.get(j).onChar[i].size();k++){
                    if(!state.currentStates.contains(firstState.currentStates.get(j).onChar[i].get(k)))
                        state.currentStates.add(firstState.currentStates.get(j).onChar[i].get(k));
                    else 
                        System.out.println("contains0");
                    if(firstState.currentStates.get(j).onChar[i].get(k).isFinal)
                        state.isFinal=true;
                }
            }
            if(state.currentStates.size()!=0){
                Character c=new Character((char)i);
                firstState.nextState_match.put(c.toString(),state);
                firstState.nextStates.add(state);
            }
        }
        return firstState;
    }
    
    public static final DFA_State generateOtherStates(DFA_State state){
        
        for(int i=0;i<state.currentStates.size();i++){
            for(int j=0;j<state.currentStates.get(i).onEmpty.size();j++){
                if(!state.currentStates.contains(state.currentStates.get(i).onEmpty.get(j)))
                    state.currentStates.add(state.currentStates.get(i).onEmpty.get(j));
                if(state.currentStates.get(i).onEmpty.get(j).isFinal)
                   state.isFinal=true;  
            }
        }
        for(int i=0;i<NFA_State.MAX_CHAR;i++){
            DFA_State nextstate=new DFA_State();
            for(int j=0;j<state.currentStates.size();j++){
                for(int k=0;k<state.currentStates.get(j).onChar[i].size();k++){
                    if(!nextstate.currentStates.contains(state.currentStates.get(j).onChar[i].get(k)))
                        nextstate.currentStates.add(state.currentStates.get(j).onChar[i].get(k));
                    //else
                       // System.out.println("contains");
                    if(state.currentStates.get(j).onChar[i].get(k).isFinal)
                        nextstate.isFinal=true;
                }
            }
            if(nextstate.currentStates.size()!=0){
                state.nextState_match.put((char)i+"", nextstate);
                state.nextStates.add(nextstate);
            }
        }
        return state;
    }
    
}
