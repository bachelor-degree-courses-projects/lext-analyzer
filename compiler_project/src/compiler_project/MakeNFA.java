
package compiler_project;

import java.util.*;

public class MakeNFA {

    private String input;
    private HashMap<String,String> vars;
    private Stack<Operators> operatorStack;
    private Stack<NFA> NFAStack;
    public ArrayList<Token> tokens;

    public MakeNFA(String input,HashMap<String,String> vars){
        this.input = input;
        this.vars = vars;
        this.NFAStack = new Stack<>();
        this.operatorStack = new Stack<>();
        this.tokens = new ArrayList<>();
        tokenize();
    }

    public String getNext(){
        if(input.charAt(0) == '['){
            String temp = input.substring(0,5);
            input = input.substring(5);
            return temp;
        }else if(input.charAt(0) == '\\'){

            List<String> varsName = new ArrayList<>();
            varsName.addAll(vars.keySet());


            for(String var : varsName){
                if (input.length() > var.length() && input.substring(1, 1 + var.length()).equals(var)) {
                    input = input.substring(1 + var.length());
                    return "\\" + var;
                }

            }

            String temp = input.substring(0,2);
            input = input.substring(2);
            return temp;

        }else {
            String temp = input.substring(0,1);
            input = input.substring(1);
            return temp;
        }
    }

    private void tokenize(){
        while (input.length()!=0){
            Token first = makeToken(getNext());
            tokens.add(first);
        }

        ArrayList<Token> temp = new ArrayList<>();
        for (int i = 0; i < tokens.size() - 1; i++){
            temp.add(tokens.get(i));
            if(isConcat(tokens.get(i),tokens.get(i+1))){
                temp.add(new Token("",Type.OPERATOR));
            }
        }

        temp.add(tokens.get(tokens.size()-1));
        tokens = temp;
    }


    private  Token makeToken(String next){
        if(next.length() == 0){
            return null;
        }
        Type type = Type.TERMINAL;
        if(next.charAt(0) == '\\' && vars.keySet().contains(next.substring(1)) ){
            type = Type.VAR;
            next = next.substring(1);
        }else if(next.charAt(0) == '\\'){
            type = Type.TERMINAL;
            next = next.substring(1);
        }else if(next.charAt(0) == '+'  || next.charAt(0) == '*' || next.charAt(0) == '|' ||
                next.charAt(0) == '.' || next.charAt(0) == '(' || next.charAt(0) == ')' ){
            type = Type.OPERATOR;
        }else if(next.charAt(0) == '['){
            type = Type.SET;
        }
        return new Token(next,type);
    }

    private boolean isConcat(Token first,Token second){
        boolean firstIsChar = (first.type == Type.SET || first.type == Type.TERMINAL || first.type == Type.VAR || (first.type == Type.OPERATOR && first.text.equals(".")));
        boolean secondIsChar = (second.type == Type.SET || second.type == Type.TERMINAL || second.type == Type.VAR || (second.type == Type.OPERATOR && second.text.equals(".")));
        if ( firstIsChar && secondIsChar) {
            return true;
        } else if ( firstIsChar && second.text.equals("(") ) {
            return true;
        } else if ( first.text.equals(")") && secondIsChar) {
            return true;
        } else if (first.text.equals("*")   && secondIsChar ) {
            return true;
        } else if ( first.text.equals("*") && second.text.equals("(")) {
            return true;
        } else if (first.text.equals("+")   && secondIsChar ) {
            return true;
        } else if ( first.text.equals("+") && second.text.equals("(")) {
            return true;
        }else if ( first.text.equals(")") && second.text.equals("(")) {
            return true;
        }
        return false;
    }


    public NFA constructNFA(){

        for (int i = 0 ; i < tokens.size() ; i++) {

            Token token = tokens.get(i);
            NFA nfa = null;
            if(token.type != Type.OPERATOR || (token.type == Type.OPERATOR && token.text.equals(".") )){
                NFAStack.add(handleToken(token));
            } else if (operatorStack.isEmpty()) {
                operatorStack.push(getOperator(token));

            } else if (token.type == Type.OPERATOR && token.text.equals("(")) {
                operatorStack.push(Operators.OPENP);

            } else if (token.type == Type.OPERATOR && token.text.equals(")")) {
                while (operatorStack.get(operatorStack.size()-1) != Operators.OPENP) {
                    doOperation();
                }

                // Pop the '(' left parenthesis
                operatorStack.pop();

            } else {
                while (!operatorStack.isEmpty() &&
                        Priority (getOperator(token), operatorStack.get(operatorStack.size() - 1)) ){
                    doOperation ();
                }
                operatorStack.push(getOperator(token));
            }
        }

        // Clean the remaining elements in the stack
        while (!operatorStack.isEmpty()) {	doOperation(); }



        return NFAStack.pop();
    }

    public NFA handleToken(Token token){
        NFA nfa = null;
        switch (token.type){

            case SET:
                nfa = NFA.charachter(token.text.charAt(1));
                for (char c = token.text.charAt(1); c <= token.text.charAt(3) ; c++){
                    NFA nfa2 = NFA.charachter(c);
                    nfa = NFA.or(nfa,nfa2);
                    
                }
                break;

            case TERMINAL:
                nfa = NFA.charachter(token.text.charAt(0));
                break;

            case VAR:
                MakeNFA mk = new MakeNFA(vars.get(token.text),vars);
                nfa =  mk.constructNFA();
                break;

            case OPERATOR:
                if(token.text.equals(".")){
                    nfa = NFA.dot();
                }
                break;
        }
        return nfa;
    }



    private void doOperation(){
        Operators op = operatorStack.pop();
        NFA nfa1 = null;
        NFA nfa2 = null;
        NFA nfa = null;
        switch (op){
            case OR:
                nfa1 = NFAStack.pop();
                nfa2 = NFAStack.pop();
                nfa =  NFA.or(nfa2,nfa1);
                break;
            case CONCAT:
                nfa1 = NFAStack.pop();
                nfa2 = NFAStack.pop();
                nfa =  NFA.concat(nfa2,nfa1);
                break;
            case STAR:
                nfa1 = NFAStack.pop();
                nfa =  NFA.repetitionStar(nfa1);
                break;
            case PLUS:
                nfa1 = NFAStack.pop();
                nfa =  NFA.repetitionPlus(nfa1);
                break;
        }
        NFAStack.push(nfa);
    }

    private static boolean Priority (Operators first,Operators second) {
        if(first == second) {	return true;	}
        if(first == Operators.STAR && second == Operators.PLUS) {	return true;	}
        if(first == Operators.PLUS && second == Operators.STAR) {	return true;	}
        if(first == Operators.STAR) 	                        {	return false;	}
        if(second == Operators.STAR) 	                        {	return true;	}
        if(first == Operators.PLUS) 	                        {	return false;	}
        if(second == Operators.PLUS) 	                        {	return true;	}
        if(first == Operators.CONCAT) 	                        {	return false;	}
        if(second == Operators.CONCAT) 	                        {	return true;	}
        if(first == Operators.OR ) 	                            {	return false;	}
        else 				                                    {	return true;	}
    }

    private Operators getOperator(Token token){
        if(token.text.length()==0){
            return Operators.CONCAT;
        }else if(token.text.equals("*")){
            return Operators.STAR;
        }else if(token.text.equals("+")){
            return Operators.PLUS;
        }else if(token.text.equals("(")){
            return Operators.OPENP;
        }else if(token.text.equals(")")){
            return Operators.CLOSEP;
        }else{
            return Operators.OR;
        }
    }

}

class LengthComparator implements java.util.Comparator<String> {

    public int compare(String s1, String s2) {
        return s2.length() - s1.length();
    }
}

enum Operators{
    STAR,PLUS,CONCAT,OR,OPENP,CLOSEP
}

enum Type{
    TERMINAL, OPERATOR,VAR,SET
}

class Token{
    String text;
    Type type;

    public Token(String text,Type type){
        this.text = text;
        this.type = type;
    }

    @Override
    public String toString() {
        return text + " : " + type ;
    }
}
