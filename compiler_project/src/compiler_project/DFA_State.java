
package compiler_project;


import java.util.ArrayList;
import java.util.HashMap;

public class DFA_State {
    ArrayList<NFA_State> currentStates;
    HashMap<String,DFA_State> nextState_match;
    ArrayList<DFA_State> nextStates;
    boolean isFinal=false;
    
    public DFA_State(){
        currentStates=new ArrayList<NFA_State>();
        nextState_match=new HashMap<String,DFA_State>();
        nextStates=new ArrayList<DFA_State>();
    }
    
}
