
package compiler_project;

public class NFA {
    public NFA_State entryState;
    public NFA_State exitState;
    
    public NFA(NFA_State entry,NFA_State exit){
        this.entryState=entry;
        this.exitState=exit;
    }

    NFA() {
    }
    
    public boolean matches(String string){
        return entryState.matches(string);
    }
    
    public static final NFA charachter(char c){
        NFA_State entry=new NFA_State();
        NFA_State exit=new NFA_State();
        entry.addCharEdge(c, exit);
        exit.isFinal=true;
        return new NFA(entry, exit);
    }
    
    public static final NFA emptyEdge(){
        NFA_State entry=new NFA_State();
        NFA_State exit=new NFA_State();
        entry.addEmptyEdge(exit);
        exit.isFinal=true;
        return new NFA(entry,exit);
    }
    
    public static final NFA repetitionStar(NFA nfa){
        nfa.exitState.addEmptyEdge(nfa.entryState);
        nfa.entryState.addEmptyEdge(nfa.exitState);

        return nfa;
    }
    
    public static final NFA repetitionPlus(NFA nfa){
        nfa.entryState.isFinal = false;
        nfa.exitState.addEmptyEdge(nfa.entryState);
        return nfa;      
    }
            
    public static final NFA dot(){
        NFA_State entry=new NFA_State();
        NFA_State exit=new NFA_State();
        char startChar='A';
        char endChar='z';
        for(char i=startChar;i<=endChar;i++)
            entry.addCharEdge(i, exit);
        startChar='0';
        endChar='9';
        for(char i=startChar;i<=endChar;i++)
            entry.addCharEdge(i, exit);
        entry.addCharEdge('_', exit);
        entry.addCharEdge(' ', exit);
        exit.isFinal=true;
        return new NFA(entry,exit);
        
    }
    
    public static final NFA concat(NFA firstNFA,NFA secondNFA){
        firstNFA.exitState.isFinal=false;
        secondNFA.exitState.isFinal=true;
        firstNFA.exitState.addEmptyEdge(secondNFA.entryState);
        return new NFA(firstNFA.entryState,secondNFA.exitState);
    }
    
    public static final NFA or(NFA firstChoice,NFA secondChoice){
        NFA_State entry=new NFA_State();
        NFA_State exit=new NFA_State();
        firstChoice.exitState.isFinal=false;
        secondChoice.exitState.isFinal=false;
        entry.addEmptyEdge(firstChoice.entryState);
        entry.addEmptyEdge(secondChoice.entryState);
        firstChoice.exitState.addEmptyEdge(exit);
        secondChoice.exitState.addEmptyEdge(exit);
        exit.isFinal = true;
        entry.isFinal = false;
        return new NFA(entry,exit);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
